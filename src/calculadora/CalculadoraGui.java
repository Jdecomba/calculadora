package calculadora;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
 
public class CalculadoraGui extends JFrame { 
    String operator = "";
    int numLeftParentheses=0,numRightParentheses=0;
    JButton zero = new JButton(" 0 ");
    JButton one = new JButton("  1 ");
    JButton two = new JButton(" 2 ");
    JButton three = new JButton(" 3 ");
    JButton four = new JButton(" 4 ");
    JButton five = new JButton(" 5 ");
    JButton six = new JButton(" 6 ");
    JButton seven = new JButton(" 7 ");
    JButton eight = new JButton(" 8 ");
    JButton nine = new JButton(" 9 ");
    JButton plus = new JButton(" + ");
    JButton minus = new JButton(" - ");
    JButton times = new JButton(" * ");
    JButton divide = new JButton(" / ");
    JButton log = new JButton("log");
    JButton percent = new JButton("%");
    JButton pow = new JButton(" ^ ");
    JButton pow2 = new JButton(" ^2 ");
    JButton sqrt = new JButton("2√");
    JButton squaren = new JButton("y√x");
    JButton pi = new JButton("π");
    JButton euler = new JButton("e");
    JButton equals = new JButton(" = ");
    JButton point = new JButton(" . ");
    JButton clear = new JButton("clr");
    JButton parentheses1 = new JButton(" ( "); 
    JButton parentheses2 = new JButton(" ) "); 
    JButton off = new JButton("Off");
    JTextArea inputArea = new JTextArea(3, 5);
    JTextField resultArea = new JTextField();
    Operations operations = new Operations();
    Solver solver = new Solver();
    List<String> allOperations= new ArrayList<>();
    List<String> operationInputs= new ArrayList<>();
    List<String> allInputs= new ArrayList<>();    
    
    public static void main(String[] args) {        
        CalculadoraGui calculatorGUI = new CalculadoraGui();                        
        calculatorGUI.setSize(300, 340);
        calculatorGUI.setTitle(" Calculator ");
        calculatorGUI.setResizable(false);
        calculatorGUI.setVisible(true);
        calculatorGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
    }
     
    public CalculadoraGui() {
        inputArea.setForeground(Color.BLACK);
        inputArea.setBackground(Color.WHITE);
        inputArea.setLineWrap(true);
        inputArea.setWrapStyleWord(true);
        inputArea.setEditable(false);
        inputArea.setFont(inputArea.getFont().deriveFont(15f));
        add(new JScrollPane(inputArea), BorderLayout.NORTH);
        
        resultArea.setEditable(false);
        resultArea.setFont(resultArea.getFont().deriveFont(20f));
        resultArea.setHorizontalAlignment(SwingConstants.RIGHT);        
        resultArea.setForeground(Color.BLACK);
        resultArea.setBackground(Color.WHITE);                
        resultArea.setEditable(false);        
        add(resultArea,BorderLayout.CENTER);  
        
        JPanel buttonpanel = new JPanel();
        buttonpanel.setLayout(new GridLayout(8, 0));      
        
        clear.setBackground(new java.awt.Color(60,179,113));
        buttonpanel.add(clear);        
        parentheses1.setBackground(new java.awt.Color(60,179,113));
        buttonpanel.add(parentheses1);
        parentheses2.setBackground(new java.awt.Color(60,179,113));
        buttonpanel.add(parentheses2);        
        off.setBackground(Color.RED);
        buttonpanel.add(off);
        
        log.setBackground(Color.ORANGE);
        buttonpanel.add(log);
        percent.setBackground(Color.ORANGE);
        buttonpanel.add(percent);
        pow.setBackground(Color.ORANGE);
        buttonpanel.add(pow);
        pow2.setBackground(Color.ORANGE);
        buttonpanel.add(pow2);
                
        buttonpanel.add(pi);        
        buttonpanel.add(euler);
        squaren.setBackground(Color.ORANGE);
        buttonpanel.add(squaren);
        sqrt.setBackground(Color.ORANGE);
        buttonpanel.add(sqrt);
        
        buttonpanel.add(seven);
        buttonpanel.add(eight);
        buttonpanel.add(nine);
        divide.setBackground(Color.ORANGE);
        buttonpanel.add(divide);
        
        buttonpanel.add(four);
        buttonpanel.add(five);
        buttonpanel.add(six);
        times.setBackground(Color.ORANGE);
        buttonpanel.add(times);
        
        buttonpanel.add(one);
        buttonpanel.add(two);
        buttonpanel.add(three);
        minus.setBackground(Color.ORANGE);
        buttonpanel.add(minus);
        
        buttonpanel.add(point);
        buttonpanel.add(zero);
        equals.setBackground(new java.awt.Color(60,179,113));
        buttonpanel.add(equals);
        plus.setBackground(Color.ORANGE);
        buttonpanel.add(plus);        
 
        //add(buttonpanel, BorderLayout.CENTER);
        add(buttonpanel,BorderLayout.SOUTH);
        
        allOperations.add("log");
        allOperations.add("√");
        allOperations.add("^");
        allOperations.add("%");
        allOperations.add("*");
        allOperations.add("/");
        allOperations.add("+");
        allOperations.add("-");
        allOperations.add("(");
        allOperations.add(")");
                
        zero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("0".toString());
                allInputs.add("0");
            }
        });
 
        one.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("1".toString());
                allInputs.add("1");
            }
        });
 
        two.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("2".toString());
                allInputs.add("2");
            }
        });
 
        three.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("3".toString());
                allInputs.add("3");
            }
        });
 
        four.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("4".toString());
                allInputs.add("4");
            }
        });
 
        five.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("5".toString());
                allInputs.add("5");
            }
        });
 
        six.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("6".toString());
                allInputs.add("6");
            }
        });
 
        seven.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("7".toString());
                allInputs.add("7");
            }
        });
 
        eight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("8".toString());
                allInputs.add("8");
            }
        });
 
        nine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("9".toString());
                allInputs.add("9");
            }
        });
 
        parentheses1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("(".toString());
                allInputs.add("(");
                numLeftParentheses++;
            }
        });

        parentheses2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append(")".toString());
                allInputs.add(")");
                numRightParentheses++;
            }
        });
        
        plus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("+".toString());
                operationInputs.add("+");
                allInputs.add("+");
            }
        });
 
        minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("-".toString());
                operationInputs.add("-");
                allInputs.add("-");
            }
        });
 
        times.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("*".toString());
                operationInputs.add("*");
                allInputs.add("*");
            }
        });
 
        divide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("/".toString());
                operationInputs.add("/");
                allInputs.add("/");
            }
        });

        percent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("%".toString());
                operationInputs.add("%");
                allInputs.add("%");
            }
        });          
        
        log.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("log".toString());
                operationInputs.add("log");
                allInputs.add("log");
            }
        });        
        
        pow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("^".toString());
                operationInputs.add("^");
                allInputs.add("^");
            }
        });

        pow2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("^2".toString());
                operationInputs.add("^");
                allInputs.add("^");
                allInputs.add("2");
            }
        });
        
        squaren.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("√".toString());
                operationInputs.add("√");
                allInputs.add("√");
            }
        });

        sqrt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("√2".toString());                
                operationInputs.add("√");
                allInputs.add("√");
                allInputs.add("2");
            }
        });
        
        pi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("π".toString());
                allInputs.add("π");
            }
        });

        euler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append("e".toString());
                allInputs.add("e");
            }
        });
                
        equals.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {                                             
                try {
                    if(inputArea.getText().isEmpty()){
                        inputArea.setText("");
                        resultArea.setText("");
                    }else{                                         
                        String equationResult;                        
                        System.out.println("\n"+"[Starting new Equation]");   
                        System.out.println("Operators: " + operationInputs);
                        System.out.println("Inputs: " + allInputs +"\n");                        
                        if(operator.equals(".") && operationInputs.isEmpty()){                            
                            equationResult = operations.Fraction(Double.parseDouble(inputArea.getText()));
                            //inputArea.append(" = " + equationResult);
                            resultArea.setText("= "+equationResult);                            
                            System.out.println("Result: " + equationResult);
                        }else{
                            //inputArea.setText(parseEquation(inputArea.getText()));
                            resultArea.setText(parseEquation());
                        }                        
                    }                   
                } catch (Exception e) {                    
                    //outputArea.setText(" Syntax Error ");
                    System.out.println(e);
                }
            }
        });
 
        point.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.append(".".toString());
                if(operator.equals("")){
                    operator = ".";
                }
                allInputs.add(".");                
            }
        });
 
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                inputArea.setText("");
                resultArea.setText("");
                clearVariables();
            }
        });
        
        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent f) {
                System.exit(0);
            }
        });                                
    }

    public void clearVariables(){
        operator = "";
        operationInputs.clear();
        allInputs.clear();
        numLeftParentheses=0;                
        numRightParentheses=0;
    }
    
    public String parseEquation(){
        String equationResult;        
        //Check if there's any operation
        if(!operationInputs.isEmpty()){
            //Check if it doesn't have parentheses
            if(numLeftParentheses==0 && numRightParentheses==0){
                equationResult = "= "+solver.solveEquation(operationInputs,allInputs,allOperations,operations);
                System.out.println("Result: " + equationResult);        
            }
            //Check if it have parentheses
            else if((numLeftParentheses==numRightParentheses) && numLeftParentheses!=0 && numRightParentheses!=0){
                int posLParentheses=0, posRParentheses=0;                                    
                List<String> subEquation;
                List<String> subOperations;                                                      
                //While it has parentheses
                while(numLeftParentheses>0){
                    subEquation = new ArrayList<>();
                    subOperations = new ArrayList<>();                    
                    //Get positions for the first parentheses
                    for (int i = 0; i < allInputs.size(); i++) {
                        if(allInputs.get(i).equals("(")){
                            posLParentheses = i;
                        }else if(allInputs.get(i).equals(")")){
                            posRParentheses = i;
                            break;
                        }
                    }                                        
                    //Get Equation inside parentheses                                         
                    subEquation = new ArrayList<>(allInputs.subList(posLParentheses+1, posRParentheses));                                        
                    //subEquation = allInputs.subList(posLParentheses+1, posRParentheses);
                    System.out.println("Equation inside parenthesis: " + subEquation);                                        
                    //Get Operations of the Equation
                    for (int i = 0; i < subEquation.size(); i++) {                                        
                        if(allOperations.contains(subEquation.get(i))){
                            subOperations.add(subEquation.get(i));
                        }
                    }                                                               
                    //Solve Equation
                    equationResult = solver.solveEquation(subOperations,subEquation,allOperations,operations);                                        
                    //Put the result in the inputs                    
                    allInputs.set(posRParentheses,equationResult);                                                                                 
                    //Replace Equation with the result
                    for (int i = posLParentheses; i < posRParentheses; i++) {
                        allInputs.remove(posLParentheses);
                    }                    
                    System.out.println("After Solver: " + allInputs + "\n"); 
                    numLeftParentheses--; 
                }                                    
                System.out.println("Without Parentheses: " + allInputs);                                    
                if(allInputs.size()>1){                                        
                    subOperations = new ArrayList<>();                                        
                    //Get remaining operations
                    for (int i = 0; i < allInputs.size(); i++) {                                        
                        if(allOperations.contains(allInputs.get(i))){
                            subOperations.add(allInputs.get(i));
                        }
                    }                    
                    //Solve remaining equation      
                    equationResult = "= "+solver.solveEquation(subOperations,allInputs,allOperations,operations);
                }
                else{
                    //Get the input remaining
                    equationResult = "= "+allInputs.get(0);                                        
                }            
            }else{
                equationResult = " Syntax Error ";                
            }                                                                  
        }else{
            equationResult = "";
            clearVariables();
        }                                                                
        return equationResult;
    }
} 