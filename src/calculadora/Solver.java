package calculadora;

import java.util.List;

public class Solver {
    
    /**
     * Solves a given equation.  If the list <tt>operationInputs</tt>  
     * has more than 1 operation, it solves it. 
     * Otherwise, it returns the first given input from the list <tt>allInputs</tt>.  
     *
     * @return a <tt>String</tt> from the result of the equation.
     */
    public String solveEquation(List<String> operationInputs, List<String> allInputs, List<String> allOperations, Operations operations){
        Boolean goodSyntax=true;
        String result = "";
        
        //While there's some operation
        while(!operationInputs.isEmpty() || allInputs.size()!=1){ 
            int posOperator,posInput;
            Double n1;
            String currOperator;                            
            //If the operation is log
            if(operationInputs.contains("log")){ 
                posOperator = operationInputs.indexOf("log");
                posInput = allInputs.indexOf("log");

                currOperator = operationInputs.get(posOperator);                                 

                String number = "";

                for (int i = posInput+1; i < allInputs.size(); i++) {
                    if(allOperations.contains(allInputs.get(i))){
                        break;
                    }else{
                        number += allInputs.get(i);
                    }
                }

                n1 = parseNumber(number);

                System.out.println("Operation: " + currOperator + n1);                                 
                operationInputs.remove(posOperator);                                                                                               
                allInputs.set(posInput,String.valueOf(operations.Logarithm(n1)));                                                                                                 

                for (int i = 0; i < number.length(); i++) {
                    allInputs.remove(posInput+1);
                }                                                                
            }
            //If the operation is root or power   
            else if(operationInputs.contains("√") || operationInputs.contains("^")){                                                                        
                goodSyntax = solveOperation("√","^",operationInputs,allInputs,allOperations,operations);                                        
            }
            //If the operation is percent
            else if(operationInputs.contains("%")){
                goodSyntax = solveOperation("%","?",operationInputs,allInputs,allOperations,operations);                                                                            
            }
            //If the operation is multiply or divide
            else if(operationInputs.contains("*") || operationInputs.contains("/")){
                goodSyntax = solveOperation("*","/",operationInputs,allInputs,allOperations,operations);                                        
            }
            //If the operation is add or subtract
            else if(operationInputs.contains("+") || operationInputs.contains("-")){
                goodSyntax = solveOperation("+","-",operationInputs,allInputs,allOperations,operations);                                                                                                                                                                                                                                                                      
            }
            //If something went wrong with the operation, set the result as 'Math Error'   
            if(goodSyntax == false){
                result = "Math Error";
                break;
            }            
        }
        //If all went good with the operation, get the result from allInputs
        if(goodSyntax == true){
            result = allInputs.get(0);
        }
        System.out.println("Solved Equation: "+result);           
        return result;
    }
    /**
     * Solves a given operation.  Recieves 2 operations <tt>op1</tt> and <tt>op1</tt>            
     * and checks the first aparition from both of them.
     * Then it solves the leftmost operation.
     * 
     * @return a <tt>Boolean</tt> indicating if the result of the operation was successful.
     */    
    public Boolean solveOperation(String op1, String op2, List<String> operationsInputs, List<String> allInputs, List<String> allOperations, Operations operations){
        int posOperator,posInput,firstOpPos,secondOpPos,count1 = 0,count2 = 0;
        Double n1, n2;
        String operator,number1 = "", number2 = "";
        
        firstOpPos = operationsInputs.indexOf(op1);
        secondOpPos = operationsInputs.indexOf(op2);
        
        //Get the leftmost operator position from operationsInputs
        if((firstOpPos != -1) && (secondOpPos != -1)){
            posOperator = Math.min(firstOpPos,secondOpPos);                                                                                                                                                                                
        }else if(firstOpPos == -1){
            posOperator = secondOpPos;                                    
        }else{
            posOperator = firstOpPos;
        }
        
        //Get the leftmost operator
        operator = operationsInputs.get(posOperator);
        //Get the leftmost operator index from allInputs
        posInput = allInputs.indexOf(operator);                                        
        
        //Get the first number
        for (int i = posInput-1; i > -1; i--) {
            if(allOperations.contains(allInputs.get(i))){
                break;
            }else{
                count1++;
                number1 += allInputs.get(i);
            }
        }
        
        //Get the second number
        for (int i = posInput+1; i < allInputs.size(); i++) {
            if(allOperations.contains(allInputs.get(i))){
                break;
            }else{
                count2++;
                number2 += allInputs.get(i);
            }
        }
 
        //Parses the numbers
        if(count1==1){                                    
            n1 = parseNumber(number1);                                  
        }else{
            StringBuilder sb = new StringBuilder(number1);                                 
            n1 = Double.parseDouble(sb.reverse().toString());                                                                                                 
        }                               
        n2 = parseNumber(number2);  
                                
        System.out.println("Operation: "+ n1 + operator + n2);
        //Remove the operation from the list of operations
        operationsInputs.remove(posOperator);
        
        //Get the result according with the operation
        switch (operator) {
            case "+":
                allInputs.set(posInput,String.valueOf(operations.Add(n1,n2))); 
                break;
            case "-":
                allInputs.set(posInput,String.valueOf(operations.Subtract(n1,n2)));
                break;
            case "/":
                if(n2==0){
                    return false;
                }else{
                    allInputs.set(posInput,String.valueOf(operations.Divide(n1,n2)));
                }                                
                break;
            case "*":
                allInputs.set(posInput,String.valueOf(operations.Multiply(n1,n2)));
                break;
            case "^":
                allInputs.set(posInput,String.valueOf(operations.Power(n1,n2)));
                break; 
            case "√":
                allInputs.set(posInput,String.valueOf(operations.Square(n1,n2)));
                break;                                
            case "%":
                allInputs.set(posInput,String.valueOf(operations.Percentage(n1,n2)));
                break;                
            default:
                break;
            }        
                
        //Remove second number
        for (int i = 0; i < count2; i++) {
            allInputs.remove(posInput+1);
        }                                                                

        //Remove first number
        for (int i = 1; i <= count1; i++) {
            allInputs.remove(posInput-i);
        }                                                                                                                                                                                                
        
        return true;
    }
    /**
     * Parses a given String to Double.  Recieves a String <tt>n</tt> and according
     * with its content, returns the value.
     * 
     * @return a <tt>Double</tt> with the according value.
     */    
    public Double parseNumber(String n){
        Double number;
        //If the number is PI
        if(n.equals("π")){
            number=Math.PI;
        }
        //If the number is Euler
        else if(n.equals("e")){
            number=Math.E;
        }
        //If is another number
        else{
            number = Double.parseDouble(n);
        }      
        return number;
    }    
}
