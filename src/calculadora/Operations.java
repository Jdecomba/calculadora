package calculadora;

public class Operations {
	
    public double Add(double n1 ,double n2){
	   return n1+n2;
    }
    
    public double Subtract(double n1 ,double n2){
	   return n1-n2;
    }
    
    public double Multiply(double n1 ,double n2){
	   return n1*n2;
    }
    
    public double Divide(double n1 ,double n2){
           return n1/n2;
    }

    public double Power(double n1 ,double n2){
	   return Math.pow(n1, n2);
    }
    
    public double Square(double n1 ,double n2){           
	   return Power(n1,(1.0/n2));
    }        	
    
    public double Logarithm(double n1){           
	   return (Math.log(n1)/Math.log(10));
    }    
    
    public double Percentage(double n1 ,double n2){           
	   return ((n1/n2)*100);
    }
    
    public String Fraction(double n){           
        if (n < 0){
            return "-" + Fraction(-n);
        }
        double tolerance = 1.0E-6;
        double h1=1; double h2=0;
        double k1=0; double k2=1;
        double b = n;
        do {
            double a = Math.floor(b);
            double aux = h1; h1 = a*h1+h2; h2 = aux;
            aux = k1; k1 = a*k1+k2; k2 = aux;
            b = 1/(b-a);
        } while (Math.abs(n-h1/k1) > n*tolerance);
        return h1+"/"+k1;
    }        	
}