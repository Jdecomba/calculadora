# CALCULADORA - Proyecto Semestral de Testing

Projecto Semestral para el ramo de Testing y Calidad se Software de la Universidad Andrés Bello

## Getting Started

Clonar el repositorio utilizando 

```
git clone https://Jdecomba@bitbucket.org/Jdecomba/calculadora.git
```

o descargar el archivo comprimido

### Prerequisites

Java
NetBeans

### Installing

Abrir el proyecto el NEtBeans y correrlo.

## Authors

* **Juan José de Comba** - *Desarollo* - [Mail](jdecomba@gmail.com)
* **Nicolás Balbontín** - *Desarollo* - [Mail](nicolas.balbontin@gmail.com)